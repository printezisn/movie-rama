﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using MovieRama.Core;
using MovieRama.Core.Services;
using MovieRama.EF;

namespace MovieRama.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            AddMappings();

            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        void AddMappings()
        {
            Resolver.Instance.Map<IMovieRamaRepository, EFMovieRamaRepository>();
        }
    }
}