﻿/// <reference path="C:\Users\Solidus\Documents\Visual Studio 2015\Projects\MovieRama\MovieRama.Web\Content/assets/js/jquery.validate.min.js" />
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            options: {
                rebase: true
            },
            sitecss: {
                files: {
                    'Content/assets/site.min.css': [
                        'Content/assets/css/bootstrap.min.css',
                        'Content/assets/css/font-awesome.min.css',
                        'Content/assets/css/toastr.min.css',
                        'Content/assets/css/site.css'
                    ]
                }
            }
        },
        babel: {
            options: {
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            jsx: {
                files: [{
                    expand: true,
                    cwd: 'Content/assets/jsx/',
                    src: ['*.jsx'],
                    dest: 'Content/assets/js/react/',
                    ext: '.js'
                }]
            }
        },
        uglify: {
            options: {
                compress: true
            },
            applib: {
                src: [
                    'Content/assets/js/jquery.min.js',
                    'Content/assets/js/react.js',
                    'Content/assets/js/react-dom.js',
                    'Content/assets/js/bootstrap.min.js',
                    'Content/assets/js/bootbox.min.js',
                    'Content/assets/js/jquery.validate.min.js',
                    'Content/assets/js/jquery.validate.unobtrusive.min.js',
                    'Content/assets/js/toastr.min.js',
                    'Content/assets/js/main.js',
                    'Content/assets/js/react/vote.js'
                ],
                dest: 'Content/assets/site.min.js'
            }
        }
    });

    // Default task.  
    grunt.registerTask('default', ['babel:jsx', 'uglify', 'cssmin']);
};