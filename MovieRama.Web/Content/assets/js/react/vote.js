'use strict';

var MovieVote = React.createClass({
    displayName: 'MovieVote',

    getInitialState: function getInitialState() {
        return {
            userLiked: this.props.userLiked,
            userHated: this.props.userHated,
            likes: this.props.likes,
            hates: this.props.hates,
            loading: false
        };
    },
    like: function like() {
        var self = this;
        self.state.loading = true;
        self.forceUpdate();

        $.ajax({
            url: '/api/movies/' + MovieRamaApp.culture + '/like/' + this.props.movieId,
            type: 'POST',
            dataType: 'json',
            success: function success(data) {
                if (data.Errors) {
                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }
                } else {
                    self.setState({
                        userLiked: true,
                        userHated: false,
                        likes: data.TotalLikes,
                        hates: data.TotalHates,
                        loading: false
                    });
                }
            },
            error: function error(xhr) {
                if (xhr.status === 401) {
                    window.location = '/' + MovieRamaApp.culture + '/auth/login';
                }
            }
        });
    },
    hate: function hate() {
        var self = this;
        self.state.loading = true;
        self.forceUpdate();

        $.ajax({
            url: '/api/movies/' + MovieRamaApp.culture + '/hate/' + this.props.movieId,
            type: 'POST',
            dataType: 'json',
            success: function success(data) {
                if (data.Errors) {
                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }
                } else {
                    self.setState({
                        userLiked: false,
                        userHated: true,
                        likes: data.TotalLikes,
                        hates: data.TotalHates,
                        loading: false
                    });
                }
            },
            error: function error(xhr) {
                if (xhr.status === 401) {
                    window.location = '/' + MovieRamaApp.culture + '/auth/login';
                }
            }
        });
    },
    removeVote: function removeVote() {
        var self = this;
        self.state.loading = true;
        self.forceUpdate();

        $.ajax({
            url: '/api/movies/' + MovieRamaApp.culture + '/unvote/' + this.props.movieId,
            type: 'POST',
            dataType: 'json',
            success: function success(data) {
                if (data.Errors) {
                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }
                } else {
                    self.setState({
                        userLiked: false,
                        userHated: false,
                        likes: data.TotalLikes,
                        hates: data.TotalHates,
                        loading: false
                    });
                }
            },
            error: function error(xhr) {
                if (xhr.status === 401) {
                    window.location = '/' + MovieRamaApp.culture + '/auth/login';
                }
            }
        });
    },
    getLikesMessage: function getLikesMessage(num) {
        if (num !== 1) {
            return MovieRamaLocalization.likesMessage.replace('{0}', num);
        }

        return MovieRamaLocalization.likeMessage;
    },
    getHatesMessage: function getHatesMessage(num) {
        if (num !== 1) {
            return MovieRamaLocalization.hatesMessage.replace('{0}', num);
        }

        return MovieRamaLocalization.hateMessage;
    },
    render: function render() {
        if (this.state.loading) {
            return React.createElement('i', { className: 'fa fa-refresh fa-spin' });
        }

        var canUserVote = this.props.userId && this.props.userId !== this.props.movieCreatorId;

        if (this.state.likes === 0 && this.state.hates === 0 && canUserVote) {
            return React.createElement(
                'p',
                null,
                MovieRamaLocalization.beTheFirstToVoteForMovie,
                ':  ',
                React.createElement(
                    'a',
                    { href: 'javascript:void(0)', onClick: this.like },
                    MovieRamaLocalization.likeAction
                ),
                '  |  ',
                React.createElement(
                    'a',
                    { href: 'javascript:void(0)', onClick: this.hate },
                    MovieRamaLocalization.hateAction
                )
            );
        }

        var likeContainer = '',
            hateContainer = '';
        if (this.state.userLiked || !canUserVote) {
            likeContainer = React.createElement(
                'span',
                null,
                this.getLikesMessage(this.state.likes)
            );
        } else {
            likeContainer = React.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: this.like },
                this.getLikesMessage(this.state.likes)
            );
        }
        if (this.state.userHated || !canUserVote) {
            hateContainer = React.createElement(
                'span',
                null,
                this.getHatesMessage(this.state.hates)
            );
        } else {
            hateContainer = React.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: this.hate },
                this.getHatesMessage(this.state.hates)
            );
        }

        var rightContainer = '';
        if (this.state.userLiked && canUserVote) {
            rightContainer = React.createElement(
                'p',
                null,
                MovieRamaLocalization.youLikeThisMovie,
                '  |  ',
                React.createElement(
                    'a',
                    { href: 'javascript:void(0)', onClick: this.removeVote },
                    MovieRamaLocalization.unlike
                )
            );
        } else if (this.state.userHated && canUserVote) {
            rightContainer = React.createElement(
                'p',
                null,
                MovieRamaLocalization.youHateThisMovie,
                '  |  ',
                React.createElement(
                    'a',
                    { href: 'javascript:void(0)', onClick: this.removeVote },
                    MovieRamaLocalization.unhate
                )
            );
        }

        return React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
                'div',
                { className: 'col-sm-6' },
                likeContainer,
                '  |  ',
                hateContainer
            ),
            React.createElement(
                'div',
                { className: 'col-sm-6 hidden-xs text-right' },
                rightContainer
            ),
            React.createElement(
                'div',
                { className: 'col-sm-6 visible-xs' },
                rightContainer
            ),
            React.createElement('div', { className: 'clearfix' })
        );
    }
});
