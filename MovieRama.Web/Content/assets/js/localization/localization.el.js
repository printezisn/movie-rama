﻿var MovieRamaLocalization = {
    likesMessage: '{0} "Μου αρέσει"',
    likeMessage: '1 "Μου αρέσει"',
    hatesMessage: '{0} "Τη μισώ"',
    hateMessage: '1 "Τη μισώ"',
    likeAction: 'Μου αρέσει',
    hateAction: 'Τη μισώ',
    youLikeThisMovie: 'Σας αρέσει αυτή η ταινία',
    youHateThisMovie: 'Μισείτε αυτή την ταινία',
    beTheFirstToVoteForMovie: 'Γίνεται ο πρώτος που θα ψηφίσει για αυτή την ταινία',
    unlike: 'Δε μου αρέσει',
    unhate: 'Δε τη μισώ'
}