﻿var MovieRamaLocalization = {
    likesMessage: '{0} likes',
    likeMessage: '1 like',
    hatesMessage: '{0} hates',
    hateMessage: '1 hate',
    likeAction: 'Like',
    hateAction: 'Hate',
    youLikeThisMovie: 'You like this movie',
    youHateThisMovie: 'You hate this movie',
    beTheFirstToVoteForMovie: 'Be the first to vote for this movie',
    unlike: 'Unlike',
    unhate: 'Unhate'
}