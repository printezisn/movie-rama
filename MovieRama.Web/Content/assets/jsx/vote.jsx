﻿var MovieVote = React.createClass({
    getInitialState: function () {
        return {
            userLiked: this.props.userLiked,
            userHated: this.props.userHated,
            likes: this.props.likes,
            hates: this.props.hates,
            loading: false,
        };
    },
    like: function () {
        var self = this;
        self.state.loading = true;
        self.forceUpdate();

        $.ajax({
            url: '/api/movies/' + MovieRamaApp.culture + '/like/' + this.props.movieId,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.Errors) {
                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }
                }
                else {
                    self.setState({
                        userLiked: true,
                        userHated: false,
                        likes: data.TotalLikes,
                        hates: data.TotalHates,
                        loading: false
                    });
                }
            },
            error: function (xhr) {
                if (xhr.status === 401) {
                    window.location = '/' + MovieRamaApp.culture + '/auth/login';
                }
            }
        });
    },
    hate: function () {
        var self = this;
        self.state.loading = true;
        self.forceUpdate();

        $.ajax({
            url: '/api/movies/' + MovieRamaApp.culture + '/hate/' + this.props.movieId,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.Errors) {
                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }
                }
                else {
                    self.setState({
                        userLiked: false,
                        userHated: true,
                        likes: data.TotalLikes,
                        hates: data.TotalHates,
                        loading: false
                    });
                }
            },
            error: function (xhr) {
                if (xhr.status === 401) {
                    window.location = '/' + MovieRamaApp.culture + '/auth/login';
                }
            }
        });
    },
    removeVote: function () {
        var self = this;
        self.state.loading = true;
        self.forceUpdate();

        $.ajax({
            url: '/api/movies/' + MovieRamaApp.culture + '/unvote/' + this.props.movieId,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.Errors) {
                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }
                }
                else {
                    self.setState({
                        userLiked: false,
                        userHated: false,
                        likes: data.TotalLikes,
                        hates: data.TotalHates,
                        loading: false
                    });
                }
            },
            error: function (xhr) {
                if (xhr.status === 401) {
                    window.location = '/' + MovieRamaApp.culture + '/auth/login';
                }
            }
        });
    },
    getLikesMessage: function (num) {
        if (num !== 1) {
            return MovieRamaLocalization.likesMessage.replace('{0}', num);
        }

        return MovieRamaLocalization.likeMessage;
    },
    getHatesMessage: function (num) {
        if (num !== 1) {
            return MovieRamaLocalization.hatesMessage.replace('{0}', num);
        }

        return MovieRamaLocalization.hateMessage;
    },
    render: function () {
        if (this.state.loading) {
            return <i className="fa fa-refresh fa-spin"></i>;
        }

        var canUserVote = (this.props.userId && this.props.userId !== this.props.movieCreatorId);

        if (this.state.likes === 0 && this.state.hates === 0 && canUserVote) {
            return (
                <p>
                    {MovieRamaLocalization.beTheFirstToVoteForMovie}:
                    &nbsp;
                    <a href="javascript:void(0)" onClick={this.like}>{MovieRamaLocalization.likeAction}</a>
                    &nbsp;
                    |
                    &nbsp;
                    <a href="javascript:void(0)" onClick={this.hate}>{MovieRamaLocalization.hateAction}</a>
                </p>
            );
        }

        var likeContainer = '', hateContainer = '';
        if (this.state.userLiked || !canUserVote) {
            likeContainer = <span>{this.getLikesMessage(this.state.likes)}</span>;
        }
        else {
            likeContainer = <a href="javascript:void(0)" onClick={this.like }>{this.getLikesMessage(this.state.likes)}</a>
        }
        if (this.state.userHated || !canUserVote) {
            hateContainer = <span>{this.getHatesMessage(this.state.hates)}</span>;
        }
        else {
            hateContainer = <a href="javascript:void(0)" onClick={this.hate }>{this.getHatesMessage(this.state.hates)}</a>
        }

        var rightContainer = '';
        if (this.state.userLiked && canUserVote) {
            rightContainer = (
                <p>
                    {MovieRamaLocalization.youLikeThisMovie}
                    &nbsp;
                    |
                    &nbsp;
                    <a href="javascript:void(0)" onClick={this.removeVote}>{MovieRamaLocalization.unlike}</a>
                </p>
            );
        }
        else if (this.state.userHated && canUserVote) {
            rightContainer = (
                <p>
                    {MovieRamaLocalization.youHateThisMovie}
                    &nbsp;
                    |
                    &nbsp;
                    <a href="javascript:void(0)" onClick={this.removeVote}>{MovieRamaLocalization.unhate}</a>
                </p>
            );
        }

        return (
            <div className="row">
                <div className="col-sm-6">
                    {likeContainer}
                    &nbsp;
                    |
                    &nbsp;
                    {hateContainer}
                </div>
                <div className="col-sm-6 hidden-xs text-right">{rightContainer}</div>
                <div className="col-sm-6 visible-xs">{rightContainer}</div>
                <div className="clearfix"></div>
            </div>
        );
    }
});