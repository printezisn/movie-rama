﻿using MovieRama.Core;
using MovieRama.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MovieRama.Web
{
    /// <summary>
    /// This class configures the routes for the MVC web application
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Registers the routes for the MVC web application
        /// </summary>
        /// <param name="routes">The collection of routes</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            // Each page has a culture that defines the translations
            string defaultCulture = "en";
            using (IMovieRamaRepository repo = Resolver.Instance.Resolve<IMovieRamaRepository>())
            {
                var defaultLanguage = repo.GetDefaultLanguage();
                if (defaultLanguage != null)
                {
                    defaultCulture = defaultLanguage.Culture;
                }
            }

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{culture}/{controller}/{action}/{id}",
                defaults: new { culture = defaultCulture, controller = "Movies", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
