﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MovieRama.Core;
using MovieRama.Core.Services;

namespace MovieRama.Web
{
    /// <summary>
    /// This class configures the routes for the Web API rest interface
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Registers the routes for the Web API rest interface
        /// </summary>
        /// <param name="config">The http configuration for the Web API interface</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
