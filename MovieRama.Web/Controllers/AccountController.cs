﻿using MovieRama.Core.Models;
using MovieRama.Resources.Messages;
using MovieRama.Web.Code.Attributes;
using MovieRama.Web.Code.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieRama.Web.Controllers
{
    /// <summary>
    /// MVC controller for operations regarding user accounts
    /// </summary>
    [MRAuthorize]
    public class AccountController : BaseController
    {
        /// <summary>
        /// The profile page for the current user
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var profileModel = this.Repo.GetUser(User.Identity.Name);
            if (profileModel == null)
            {
                this.ShowNotification(MessagesResource.UserNotFoundErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index", "Movies");
            }

            return View(profileModel);
        }

        /// <summary>
        /// The profile page for the current user (POST operation)
        /// </summary>
        /// <param name="model">The model of the user's profile</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ProfileModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var errors = this.Repo.UpdateUser(model, User.Identity.Name);
            if (errors.Any())
            {
                this.AddModelErrors(errors);
                return View(model);
            }

            this.ShowNotification(MessagesResource.ProfileUpdatedSuccessMessage, ToastrNotificationType.Success);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// The change password page
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// The change password page (POST operation)
        /// </summary>
        /// <param name="model">The model of the change password operation</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var errors = this.Repo.ChangePassword(model, User.Identity.Name);
            if (errors.Any())
            {
                this.AddModelErrors(errors);
                return View(model);
            }

            this.ShowNotification(MessagesResource.PasswordChangedSuccessMessage, ToastrNotificationType.Success);

            return RedirectToAction("Index", "Movies");
        }
    }
}