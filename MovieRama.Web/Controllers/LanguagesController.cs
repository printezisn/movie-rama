﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieRama.Web.Controllers
{
    /// <summary>
    /// MVC controller for language operations
    /// </summary>
    public class LanguagesController : BaseController
    {
        /// <summary>
        /// Renders a drop down menu with the available languages of the application
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult Menu()
        {
            var languages = GetCachedLanguages();
            ViewBag.CurrentLanguage = languages.FirstOrDefault(f => f.Culture == CurrentCulture);

            return PartialView(languages);
        }
    }
}