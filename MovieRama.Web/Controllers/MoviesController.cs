﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieRama.EF;
using MovieRama.Core.Models;
using MovieRama.Resources.Messages;
using MovieRama.Web.Code.Enums;
using MovieRama.Web.Code.Attributes;

namespace MovieRama.Web.Controllers
{
    /// <summary>
    /// MVC controller for movie operations
    /// </summary>
    public class MoviesController : BaseController
    {
        /// <summary>
        /// The page with the list of movies
        /// </summary>
        /// <param name="searchModel">The parameters used for searching and sorting movies</param>
        /// <returns></returns>
        public ActionResult Index(MoviesSearchModel searchModel)
        {
            if (searchModel == null)
            {
                searchModel = new MoviesSearchModel();
            }
            searchModel.SanitizeProperties();
            searchModel.PageSize = DEFAULT_PAGE_SIZE;

            var model = this.Repo.GetMovies(CurrentCulture, searchModel, (CurrentUser != null) ? (int?)CurrentUser.Id : null);
            return View(model);
        }

        /// <summary>
        /// The page with the list of movies that are created by a specific user
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <param name="searchModel">The parameters used for searching and sorting movies</param>
        /// <returns></returns>
        public ActionResult ByUser(int id, MoviesSearchModel searchModel)
        {
            if (searchModel == null)
            {
                searchModel = new MoviesSearchModel();
            }
            searchModel.PageSize = DEFAULT_PAGE_SIZE;
            searchModel.UserId = id;
            searchModel.SanitizeProperties();

            var model = this.Repo.GetMovies(CurrentCulture, searchModel, (CurrentUser != null) ? (int?)CurrentUser.Id : null);
            return View("Index", model);
        }

        /// <summary>
        /// The movie creation page
        /// </summary>
        /// <returns></returns>
        [MRAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// The movie creation page (POST operation)
        /// </summary>
        /// <param name="model">The model for the new movie</param>
        /// <returns></returns>
        [MRAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MovieModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var errors = this.Repo.CreateMovie(model, User.Identity.Name);
            if (errors.Any())
            {
                this.AddModelErrors(errors);
                return View(model);
            }

            this.ShowNotification(MessagesResource.MovieCreatedSuccessMessage, ToastrNotificationType.Success);

            return RedirectToAction("Edit", new { id = model.Id });
        }

        /// <summary>
        /// The movie update page
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <param name="languageId">The id of the language. It is used to update localized values of the movie</param>
        /// <returns></returns>
        [MRAuthorize]
        public ActionResult Edit(int id, int? languageId)
        {
            ViewBag.LanguageId = languageId;

            var movie = this.Repo.GetMovie(id, languageId);
            if (movie == null)
            {
                this.ShowNotification(MessagesResource.MovieNotFoundErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index");
            }
            if (movie.CreatorId != this.CurrentUser.Id)
            {
                this.ShowNotification(MessagesResource.NotAuthorizedToEditMovieErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index");
            }

            return View(movie);
        }

        /// <summary>
        /// The movie update page (POST operation)
        /// </summary>
        /// <param name="model">The model for the movie</param>
        /// <param name="languageId">The id of the language. It is used to update localized values of the movie</param>
        /// <returns></returns>
        [MRAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MovieModel model, int? languageId)
        {
            ViewBag.LanguageId = languageId;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var movie = this.Repo.GetMovie(model.Id, languageId);
            if (movie == null)
            {
                this.ShowNotification(MessagesResource.MovieNotFoundErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index");
            }
            if (movie.CreatorId != this.CurrentUser.Id)
            {
                this.ShowNotification(MessagesResource.NotAuthorizedToEditMovieErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index");
            }

            var errors = this.Repo.UpdateMovie(model, languageId);
            if (errors.Any())
            {
                this.AddModelErrors(errors);
                return View(model);
            }

            this.ShowNotification(MessagesResource.MovieUpdatedSuccessMessage, ToastrNotificationType.Success);

            return View(model);
        }

        /// <summary>
        /// The movie deletion process
        /// </summary>
        /// <param name="id">The id of the movie to delete</param>
        /// <returns></returns>
        [MRAuthorize]
        public ActionResult Delete(int id)
        {
            var movie = this.Repo.GetMovie(id, null);
            if (movie == null)
            {
                this.ShowNotification(MessagesResource.MovieNotFoundErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index");
            }
            if (movie.CreatorId != this.CurrentUser.Id)
            {
                this.ShowNotification(MessagesResource.NotAuthorizedToDeleteMovieErrorMessage, ToastrNotificationType.Error);
                return RedirectToAction("Index");
            }

            var errors = this.Repo.DeleteMovie(id);
            if (errors.Any())
            {
                foreach (string error in errors)
                {
                    this.ShowNotification(error, ToastrNotificationType.Error);
                }
            }

            this.ShowNotification(MessagesResource.MovieDeletedSuccessMessage, ToastrNotificationType.Success);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Renders a drop down menu with the available languages of the application
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <param name="languageId">The id of the language. It is used to update localized values of the movie</param>
        /// <returns></returns>
        [MRAuthorize]
        [ChildActionOnly]
        public ActionResult Languages(int id, int? languageId)
        {
            var languages = GetCachedLanguages();
            ViewBag.MovieId = id;

            var currentLanguage = languages.FirstOrDefault(f => f.Id == languageId);
            if (currentLanguage == null)
            {
                currentLanguage = languages.FirstOrDefault(f => f.IsDefault);
            }
            if (currentLanguage == null)
            {
                currentLanguage = languages.FirstOrDefault();
            }

            ViewBag.CurrentLanguage = currentLanguage;

            return PartialView(languages);
        }
    }
}
