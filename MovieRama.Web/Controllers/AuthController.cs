﻿using MovieRama.Core.Models;
using MovieRama.Resources.Messages;
using MovieRama.Web.Code.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MovieRama.Web.Controllers
{
    /// <summary>
    /// MVC controller for authentication and registration operations
    /// </summary>
    public class AuthController : BaseController
    {
        /// <summary>
        /// The registration page
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// The registration page (POST operation)
        /// </summary>
        /// <param name="model">The model of the new user (registration model)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegistrationModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var errors = this.Repo.CreateUser(model);
            if (errors.Any())
            {
                this.AddModelErrors(errors);
                return View(model);
            }

            this.ShowNotification(MessagesResource.AccountCreatedSuccessMessage, ToastrNotificationType.Success);

            return RedirectToAction("Login", "Auth");
        }

        /// <summary>
        /// The login page
        /// </summary>
        /// <param name="returnUrl">The url to return after the login process is complete</param>
        /// <returns></returns>
        public ActionResult Login(string returnUrl)
        {
            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                ViewBag.ReturnUrl = returnUrl;
            }

            return View();
        }

        /// <summary>
        /// The login page (POST operation)
        /// </summary>
        /// <param name="model">The model for the login operation</param>
        /// <param name="returnUrl">The url to return after the login process is complete</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (string.IsNullOrWhiteSpace(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                returnUrl = Url.Action("Index", "Movies");
            }

            ViewBag.ReturnUrl = returnUrl;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var profileModel = this.Repo.GetUser(model.Username, model.Password);
            if (profileModel == null)
            {
                this.AddModelErrors(new List<string>() { MessagesResource.UsernameOrPasswordInvalidErrorMessage });
                return View(model);
            }

            FormsAuthentication.SetAuthCookie(profileModel.Username, model.RememberMe);
            this.ShowNotification(MessagesResource.LoginSuccessMessage, ToastrNotificationType.Success);

            return Redirect(returnUrl);
        }

        /// <summary>
        /// The current user is signed out
        /// </summary>
        /// <returns></returns>
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            this.ShowNotification(MessagesResource.SignOutSuccessMessage, ToastrNotificationType.Success);

            return RedirectToAction("Index", "Movies");
        }
    }
}