﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MovieRama.Resources.Messages;

namespace MovieRama.Web.Controllers.Api
{
    /// <summary>
    /// Web API controller for operations regarding movies
    /// </summary>
    [Authorize]
    public class MoviesController : BaseApiController
    {
        /// <summary>
        /// Adds a vote to a movie, for the current user
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <param name="isPositive">This parameter is true for liking and false for hating</param>
        /// <returns>It returns error messages if the process is not successful, otherwise it returns the total likes and hates of the movie</returns>
        private dynamic VoteForMovie(int id, bool isPositive)
        {
            var errors = this.Repo.VoteForMovie((CurrentUser != null) ? CurrentUser.Id : 0, id, isPositive);
            if (errors.Any())
            {
                return new { Errors = errors };
            }

            var movie = this.Repo.GetMovie(id, null);
            if (movie == null)
            {
                return new { Errors = new List<string>() { MessagesResource.MovieNotFoundErrorMessage } };
            }

            return new
            {
                movie.TotalLikes,
                movie.TotalHates
            };
        }

        /// <summary>
        /// Adds a like vote to a movie, for the current user
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <returns>It returns error messages if the process is not successful, otherwise it returns the total likes and hates of the movie</returns>
        [HttpPost]
        [Route("api/movies/{culture}/like/{id}")]
        public dynamic Like(int id)
        {
            return VoteForMovie(id, true);
        }

        /// <summary>
        /// Adds a hate vote to a movie, for the current user
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <returns>It returns error messages if the process is not successful, otherwise it returns the total likes and hates of the movie</returns>
        [HttpPost]
        [Route("api/movies/{culture}/hate/{id}")]
        public dynamic Hate(int id)
        {
            return VoteForMovie(id, false);
        }

        /// <summary>
        /// Removes a vote from a movie, for the current user
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <returns>It returns error messages if the process is not successful, otherwise it returns the total likes and hates of the movie</returns>
        [HttpPost]
        [Route("api/movies/{culture}/unvote/{id}")]
        public dynamic UnVote(int id)
        {
            var errors = this.Repo.UnVoteForMovie((CurrentUser != null) ? CurrentUser.Id : 0, id);
            if (errors.Any())
            {
                return new { Errors = errors };
            }

            var movie = this.Repo.GetMovie(id, null);
            if (movie == null)
            {
                return new { Errors = new List<string>() { MessagesResource.MovieNotFoundErrorMessage } };
            }

            return new
            {
                movie.TotalLikes,
                movie.TotalHates
            };
        }
    }
}
