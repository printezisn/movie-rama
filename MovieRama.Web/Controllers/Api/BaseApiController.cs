﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using MovieRama.Core.Models;
using MovieRama.Core.Services;

namespace MovieRama.Web.Controllers.Api
{
    /// <summary>
    /// The base class used for api controllers (Web API)
    /// </summary>
    public class BaseApiController : ApiController
    {
        private IMovieRamaRepository _repo;
        /// <summary>
        /// The instance of the application's service
        /// </summary>
        protected IMovieRamaRepository Repo
        {
            get
            {
                if (_repo == null)
                {
                    _repo = Core.Resolver.Instance.Resolve<IMovieRamaRepository>();
                }

                return _repo;
            }
        }

        /// <summary>
        /// The currently used culture 
        /// </summary>
        protected string CurrentCulture
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.ToLower();
            }
        }

        private ProfileModel _currentUser;
        /// <summary>
        /// The current logged in user
        /// </summary>
        protected ProfileModel CurrentUser
        {
            get
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return null;
                }
                if (_currentUser == null)
                {
                    _currentUser = this.Repo.GetUser(User.Identity.Name);
                }

                return _currentUser;
            }
        }

        /// <summary>
        /// Returns the available languages of the application. These languages are stored in the cache.
        /// </summary>
        /// <returns>A list of models for the available languages</returns>
        protected IEnumerable<LanguageModel> GetCachedLanguages()
        {
            if (HttpRuntime.Cache["Languages"] == null)
            {
                var languages = this.Repo.GetLanguages();
                HttpRuntime.Cache["Languages"] = languages;
            }

            return (IEnumerable<LanguageModel>)HttpRuntime.Cache["Languages"];
        }

        /// <summary>
        /// Initializes the api controller
        /// </summary>
        /// <param name="controllerContext"></param>
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            SetCulture(controllerContext);

            base.Initialize(controllerContext);
        }

        /// <summary>
        /// Sets the currently used culture
        /// </summary>
        /// <param name="controllerContext">The controller context</param>
        private void SetCulture(HttpControllerContext controllerContext)
        {
            var languages = GetCachedLanguages();

            string culture = null;
            IHttpRouteData[] routeData = (IHttpRouteData[])controllerContext.RouteData.Values.Values.FirstOrDefault();

            if (routeData != null && routeData.Any() && routeData[0].Values.ContainsKey("culture"))
            {
                culture = routeData[0].Values["culture"].ToString().ToLower().Trim();
            }
            if (string.IsNullOrWhiteSpace(culture) || !languages.Any(a => a.Culture == culture))
            {
                var defaultLanguage = languages.FirstOrDefault(f => f.IsDefault);
                culture = (defaultLanguage != null) ? defaultLanguage.Culture : "en";
            }

            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }
    }
}