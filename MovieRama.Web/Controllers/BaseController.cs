﻿using MovieRama.Core.Models;
using MovieRama.Core.Services;
using MovieRama.Web.Code.Enums;
using MovieRama.Web.Code.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;

namespace MovieRama.Web.Controllers
{
    /// <summary>
    /// The base class used for MVC controllers
    /// </summary>
    public class BaseController : Controller
    {
        protected const int DEFAULT_PAGE_SIZE = 10;

        private IMovieRamaRepository _repo;
        /// <summary>
        /// The instance of the application's service
        /// </summary>
        protected IMovieRamaRepository Repo
        {
            get
            {
                if (_repo == null)
                {
                    _repo = Core.Resolver.Instance.Resolve<IMovieRamaRepository>();
                }

                return _repo;
            }
        }

        /// <summary>
        /// The currently used culture 
        /// </summary>
        protected string CurrentCulture
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.ToLower();
            }
        }

        private ProfileModel _currentUser;
        /// <summary>
        /// The current logged in user
        /// </summary>
        protected ProfileModel CurrentUser
        {
            get
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return null;
                }
                if (_currentUser == null)
                {
                    _currentUser = this.Repo.GetUser(User.Identity.Name);
                }

                return _currentUser;
            }
        }

        /// <summary>
        /// Returns the available languages of the application. These languages are stored in the cache.
        /// </summary>
        /// <returns>A list of models for the available languages</returns>
        protected IEnumerable<LanguageModel> GetCachedLanguages()
        {
            if (HttpRuntime.Cache["Languages"] == null)
            {
                var languages = this.Repo.GetLanguages();
                HttpRuntime.Cache["Languages"] = languages;
            }

            return (IEnumerable<LanguageModel>)HttpRuntime.Cache["Languages"];
        }

        /// <summary>
        /// Disposes the controller and the service
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (_repo != null)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Initializes the controller
        /// </summary>
        /// <param name="requestContext">The request context</param>
        protected override void Initialize(RequestContext requestContext)
        {
            SetCulture(requestContext);

            base.Initialize(requestContext);
        }

        /// <summary>
        /// Returns the view of an action
        /// </summary>
        /// <param name="view">The view to return</param>
        /// <param name="model">The model to pass to the view</param>
        /// <returns>The view</returns>
        protected override ViewResult View(IView view, object model)
        {
            ViewBag.User = CurrentUser;
            return base.View(view, model);
        }

        /// <summary>
        /// Returns the view of an action
        /// </summary>
        /// <param name="viewName">The name of the view</param>
        /// <param name="masterName">The name of the master page of the view</param>
        /// <param name="model">The model to pass to the view</param>
        /// <returns>The view</returns>
        protected override ViewResult View(string viewName, string masterName, object model)
        {
            ViewBag.User = CurrentUser;
            return base.View(viewName, masterName, model);
        }

        // <summary>
        /// Sets the currently used culture
        /// </summary>
        /// <param name="requestContext">The request context</param>
        private void SetCulture(RequestContext requestContext)
        {
            var languages = GetCachedLanguages();

            string culture = null;
            if (requestContext.RouteData.Values.ContainsKey("culture"))
            {
                culture = requestContext.RouteData.Values["culture"].ToString().ToLower().Trim();
            }
            if (string.IsNullOrWhiteSpace(culture) || !languages.Any(a => a.Culture == culture))
            {
                var defaultLanguage = languages.FirstOrDefault(f => f.IsDefault);
                culture = (defaultLanguage != null) ? defaultLanguage.Culture : "en";
            }

            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }

        /// <summary>
        /// Adds model errors to the model state
        /// </summary>
        /// <param name="errors">The list of errors to add</param>
        protected void AddModelErrors(IEnumerable<string> errors)
        {
            foreach (string error in errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }

        /// <summary>
        /// Displays a notification to the front end
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="type">The type of notification</param>
        protected void ShowNotification(string message, ToastrNotificationType type)
        {
            List<ToastrNotificationModel> notifications = new List<ToastrNotificationModel>();
            if (TempData.ContainsKey("ToastrNotifications"))
            {
                notifications = (List<ToastrNotificationModel>)TempData["ToastrNotifications"];
            }

            notifications.Add(new ToastrNotificationModel(message, type));
            TempData["ToastrNotifications"] = notifications;
        }
    }
}