﻿using MovieRama.Web.Code.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieRama.Web.Code.Models
{
    /// <summary>
    /// This model class is used for the notifications that are displayed in the front end
    /// </summary>
    public class ToastrNotificationModel
    {
        /// <summary>
        /// The message to display
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The type of notification
        /// </summary>
        public ToastrNotificationType Type { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ToastrNotificationModel()
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="type">The type of notification</param>
        public ToastrNotificationModel(string message, ToastrNotificationType type)
        {
            this.Message = message;
            this.Type = type;
        }
    }
}