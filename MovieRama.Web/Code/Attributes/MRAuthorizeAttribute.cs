﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MovieRama.Web.Code.Attributes
{
    /// <summary>
    /// This attribute is used for authorizing requests and takes into account the culture that is used for localization
    /// </summary>
    public class MRAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Defines the actions for unauthorized requests
        /// </summary>
        /// <param name="filterContext">The filter context</param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Login", controller = "Auth", returnUrl = filterContext.HttpContext.Request.Url.PathAndQuery }));
        }
    }
}