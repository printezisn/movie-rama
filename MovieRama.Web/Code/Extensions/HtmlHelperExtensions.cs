﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace MovieRama.Web.Code.Extensions
{
    /// <summary>
    /// This class contains extensions for the html helper that is used in the views
    /// </summary>
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// Creates an html label
        /// </summary>
        /// <typeparam name="TModel">The type of the model</typeparam>
        /// <typeparam name="TValue">The type of the value</typeparam>
        /// <param name="html">The html helper object</param>
        /// <param name="expression">The lambda expression used to take the property of the model</param>
        /// <param name="htmlAttributes">The extra attributes to add to the label element</param>
        /// <returns>The html structure of the label</returns>
        public static MvcHtmlString MRLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName;
            if (metadata.IsRequired)
            {
                // Use an asterisk to indicate that a field is required
                resolvedLabelText += " *";
            }

            return LabelExtensions.LabelFor<TModel, TValue>(html, expression, resolvedLabelText, htmlAttributes);
        }
    }
}