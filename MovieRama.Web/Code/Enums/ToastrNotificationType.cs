﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieRama.Web.Code.Enums
{
    /// <summary>
    /// This enumeration contains the types of notifications displayed in the front end
    /// </summary>
    public enum ToastrNotificationType
    {
        Info,
        Success,
        Error
    }
}