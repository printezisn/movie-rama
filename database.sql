USE [MovieRama]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 3/22/2016 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Culture] [varchar](5) NOT NULL,
	[IsEnabled] [bit] NOT NULL CONSTRAINT [DF_Languages_IsEnabled]  DEFAULT ((0)),
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_Languages_IsDefault]  DEFAULT ((0)),
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MovieLocalizations]    Script Date: 3/22/2016 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieLocalizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MovieId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_MovieLocalizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Movies]    Script Date: 3/22/2016 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[TotalLikes] [int] NOT NULL,
	[TotalHates] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatorId] [int] NOT NULL,
	[CreationDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Movies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovieVotes]    Script Date: 3/22/2016 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieVotes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MovieId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsPositive] [bit] NOT NULL,
 CONSTRAINT [PK_MovieVotes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 3/22/2016 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](250) NOT NULL,
	[Password] [nvarchar](500) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[FirstName] [nvarchar](250) NOT NULL,
	[LastName] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreationDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Languages] ([Id], [Name], [Culture], [IsEnabled], [IsDefault]) VALUES (1, N'English', N'en', 1, 1)
INSERT [dbo].[Languages] ([Id], [Name], [Culture], [IsEnabled], [IsDefault]) VALUES (2, N'Ελληνικά', N'el', 1, 0)
ALTER TABLE [dbo].[Movies] ADD  CONSTRAINT [DF_Movies_TotalLikes]  DEFAULT ((0)) FOR [TotalLikes]
GO
ALTER TABLE [dbo].[Movies] ADD  CONSTRAINT [DF_Movies_TotalHates]  DEFAULT ((0)) FOR [TotalHates]
GO
ALTER TABLE [dbo].[Movies] ADD  CONSTRAINT [DF_Movies_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MovieVotes] ADD  CONSTRAINT [DF_MovieVotes_IsPositive]  DEFAULT ((0)) FOR [IsPositive]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsActive]  DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[MovieLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_MovieLocalizations_Languages] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[MovieLocalizations] CHECK CONSTRAINT [FK_MovieLocalizations_Languages]
GO
ALTER TABLE [dbo].[MovieLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_MovieLocalizations_Movies] FOREIGN KEY([MovieId])
REFERENCES [dbo].[Movies] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieLocalizations] CHECK CONSTRAINT [FK_MovieLocalizations_Movies]
GO
ALTER TABLE [dbo].[Movies]  WITH CHECK ADD  CONSTRAINT [FK_Movies_Users] FOREIGN KEY([CreatorId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Movies] CHECK CONSTRAINT [FK_Movies_Users]
GO
ALTER TABLE [dbo].[MovieVotes]  WITH CHECK ADD  CONSTRAINT [FK_MovieVotes_Movies] FOREIGN KEY([MovieId])
REFERENCES [dbo].[Movies] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieVotes] CHECK CONSTRAINT [FK_MovieVotes_Movies]
GO
ALTER TABLE [dbo].[MovieVotes]  WITH CHECK ADD  CONSTRAINT [FK_MovieVotes_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MovieVotes] CHECK CONSTRAINT [FK_MovieVotes_Users]
GO
