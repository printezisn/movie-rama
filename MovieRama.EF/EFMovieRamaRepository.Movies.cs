﻿using MovieRama.Core.Models;
using MovieRama.Core.Models.Enums;
using MovieRama.Resources.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.EF
{
    public partial class EFMovieRamaRepository
    {
        private static object _movieVotesLock = new object();

        /// <summary>
        /// Returns the available movies of the application
        /// </summary>
        /// <param name="culture">The culture used for getting localized values of movies</param>
        /// <param name="searchModel">The parameters used for searching and sorting movies</param>
        /// <param name="currentUserId">The id of the current user</param>
        /// <returns>A list of models for the movies found, using paging</returns>
        public MoviesListModel GetMovies(string culture, MoviesSearchModel searchModel, int? currentUserId)
        {
            MoviesListModel model = new MoviesListModel();
            model.SearchModel = searchModel;
            model.Page = searchModel.Page;
            model.PageSize = searchModel.PageSize;

            // Filter movies
            var query = Db.Movies.Where(w => !w.IsDeleted);
            if (searchModel.UserId.HasValue)
            {
                query = query.Where(w => w.CreatorId == searchModel.UserId);
            }

            int totalCount = query.Count();
            model.TotalPages = (int)Math.Ceiling(totalCount / (double)model.PageSize);
            model.TotalPages = Math.Max(model.TotalPages, 1);
            model.Page = Math.Max(model.Page, 1);
            model.Page = Math.Min(model.Page, model.TotalPages);

            // Sort movies
            switch (searchModel.SortBy)
            {
                case MovieSortType.Likes:
                    query = (searchModel.SortDirection == SortDirection.Asc)
                        ? query.OrderBy(o => o.TotalLikes)
                        : query.OrderByDescending(o => o.TotalLikes);
                    break;
                case MovieSortType.Hates:
                    query = (searchModel.SortDirection == SortDirection.Asc)
                        ? query.OrderBy(o => o.TotalHates)
                        : query.OrderByDescending(o => o.TotalHates);
                    break;
                default:
                    query = (searchModel.SortDirection == SortDirection.Asc)
                        ? query.OrderBy(o => o.CreationDateTime)
                        : query.OrderByDescending(o => o.CreationDateTime);
                    break;
            }

            // Apply paging
            var movies = query.Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();

            // Fill model values for each movie
            foreach (var movie in movies)
            {
                MovieModel result = new MovieModel();
                var localization = movie.MovieLocalizations.FirstOrDefault(f => f.Language.Culture == culture);

                result.Id = movie.Id;
                result.Title = (localization != null) ? localization.Title : movie.Title;
                result.Description = (localization != null) ? localization.Description : movie.Description;
                result.CreationDate = movie.CreationDateTime;
                result.CreatorId = movie.CreatorId;
                result.CreatorFullName = movie.User.FirstName + " " + movie.User.LastName;
                result.TotalLikes = movie.TotalLikes;
                result.TotalHates = movie.TotalHates;
                if (currentUserId.HasValue)
                {
                    var vote = Db.MovieVotes.FirstOrDefault(a => a.MovieId == movie.Id && a.UserId == currentUserId);
                    if (vote != null)
                    {
                        result.CurrentUserHasLiked = vote.IsPositive;
                        result.CurrentUserHasHated = !vote.IsPositive;
                    }
                }

                model.Collection.Add(result);
            }

            return model;
        }

        /// <summary>
        /// Indicates if a user has created a movie
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="username">The username of the user</param>
        /// <returns>Returns true if the user has created the movie, otherwise false</returns>
        public bool UserHasCreatedMovie(int movieId, string username)
        {
            return Db.Movies.Any(a => a.Id == movieId && a.User.Username == username);
        }

        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="model">The model of the new movie</param>
        /// <param name="creatorUsername">The username of the new movie's creator</param>
        /// <returns>A list of errors. If no errors occured, this list is empty. If it is successful the id in the model takes the id of the new movie</returns>
        public IEnumerable<string> CreateMovie(MovieModel model, string creatorUsername)
        {
            List<string> errors = new List<string>();

            try
            {
                // Get the default language
                var defaultLanguage = GetDefaultLanguageDbObject();
                if (defaultLanguage == null)
                {
                    errors.Add(MessagesResource.DefaultLanguageNotFoundErrorMessage);
                    return errors;
                }

                // Get the movie's creator
                var creator = Db.Users.FirstOrDefault(f => f.Username == creatorUsername);
                if (creator == null)
                {
                    errors.Add(MessagesResource.UserNotFoundErrorMessage);
                    return errors;
                }

                // Fill the movie's values
                Movie movie = new Movie();
                movie.Title = model.Title;
                movie.Description = model.Description;
                movie.CreatorId = creator.Id;
                movie.CreationDateTime = DateTime.UtcNow;

                // Fill the movie's default localized values
                MovieLocalization movieLocalization = new MovieLocalization();
                movieLocalization.LanguageId = defaultLanguage.Id;
                movieLocalization.Title = model.Title;
                movieLocalization.Description = model.Description;
                movie.MovieLocalizations.Add(movieLocalization);

                Db.Movies.Add(movie);
                Db.SaveChanges();

                model.Id = movie.Id;
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Returns a movie
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="languageId">The id of the language used for getting localized values of the movie</param>
        /// <returns>A model of the movie found, or null of it was not found</returns>
        public MovieModel GetMovie(int movieId, int? languageId)
        {
            // Get the movie entity
            Movie movie = Db.Movies.FirstOrDefault(f => f.Id == movieId && !f.IsDeleted);
            if (movie == null)
            {
                return null;
            }

            var localization = movie.MovieLocalizations.FirstOrDefault(f => f.LanguageId == languageId);

            // Fill the model values and apply localized values
            MovieModel model = new MovieModel();
            model.Id = movie.Id;
            model.Title = (localization != null) ? localization.Title : movie.Title;
            model.Description = (localization != null) ? localization.Description : movie.Description;
            model.CreatorId = movie.CreatorId;
            model.CreatorFullName = movie.User.FirstName + " " + movie.User.LastName;
            model.CreationDate = movie.CreationDateTime;
            model.TotalLikes = movie.TotalLikes;
            model.TotalHates = movie.TotalHates;

            return model;
        }

        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="model">The model of the movie</param>
        /// <param name="languageId">The id of the language. It is used to update localized values of the movie</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> UpdateMovie(MovieModel model, int? languageId)
        {
            List<string> errors = new List<string>();

            try
            {
                // Get the language used to update the localized values of the movie
                // or the default language if no language is requested
                Language language = null;
                if (languageId.HasValue)
                {
                    language = Db.Languages.FirstOrDefault(f => f.Id == languageId && f.IsEnabled);
                }
                else
                {
                    language = GetDefaultLanguageDbObject();
                }
                if (language == null)
                {
                    errors.Add(MessagesResource.LanguageNotFoundErrorMessage);
                    return errors;
                }

                // Get the movie entity
                Movie movie = Db.Movies.FirstOrDefault(f => f.Id == model.Id && !f.IsDeleted);
                if (movie == null)
                {
                    errors.Add(MessagesResource.MovieNotFoundErrorMessage);
                    return errors;
                }

                // Update the movie's localized values
                var localization = movie.MovieLocalizations.FirstOrDefault(f => f.LanguageId == language.Id);
                if (localization == null)
                {
                    localization = new MovieLocalization();
                    localization.LanguageId = language.Id;
                    movie.MovieLocalizations.Add(localization);
                }

                // If the used language is the default one, update the movie's default values too
                if (language.IsDefault)
                {
                    movie.Title = model.Title;
                    movie.Description = model.Description;
                }
                localization.Title = model.Title;
                localization.Description = model.Description;

                Db.SaveChanges();
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Deletes a movie
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> DeleteMovie(int movieId)
        {
            List<string> errors = new List<string>();

            try
            {
                Movie movie = Db.Movies.FirstOrDefault(f => f.Id == movieId && !f.IsDeleted);
                if (movie == null)
                {
                    errors.Add(MessagesResource.MovieNotFoundErrorMessage);
                    return errors;
                }

                movie.IsDeleted = true;
                Db.SaveChanges();
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Adds a vote for a movie
        /// </summary>
        /// <param name="userId">The id of the user that votes</param>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="isPositive">This parameter is true for liking or false for hating</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> VoteForMovie(int userId, int movieId, bool isPositive)
        {
            List<string> errors = new List<string>();

            try
            {
                // Get the movie entity
                Movie movie = Db.Movies.FirstOrDefault(f => f.Id == movieId && !f.IsDeleted);
                if (movie == null)
                {
                    errors.Add(MessagesResource.MovieNotFoundErrorMessage);
                    return errors;
                }

                // Get the user that votes for the movie
                // A user cannot vote for movies that he has created
                User user = Db.Users.FirstOrDefault(f => f.Id == userId);
                if (user == null)
                {
                    errors.Add(MessagesResource.UserNotFoundErrorMessage);
                    return errors;
                }
                if (movie.CreatorId == user.Id)
                {
                    errors.Add(MessagesResource.CannotVoteForOwnMovieErrorMessage);
                    return errors;
                }

                lock (_movieVotesLock)
                {
                    // Create a new vote or update the existing one
                    var movieVote = Db.MovieVotes.FirstOrDefault(f => f.UserId == userId && f.MovieId == movieId);
                    if (movieVote == null)
                    {
                        movieVote = new MovieVote();
                        movieVote.MovieId = movieId;
                        movieVote.UserId = userId;
                        Db.MovieVotes.Add(movieVote);
                    }

                    movieVote.IsPositive = isPositive;

                    Db.SaveChanges();

                    UpdateMovieVotes(movie);
                }
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Removes a vote from a movie
        /// </summary>
        /// <param name="userId">The id of the user to remove the vote for</param>
        /// <param name="movieId">The id of the movie</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> UnVoteForMovie(int userId, int movieId)
        {
            List<string> errors = new List<string>();

            try
            {
                lock (_movieVotesLock)
                {
                    // Remove the vote of the user
                    var movieVote = Db.MovieVotes.FirstOrDefault(f => f.UserId == userId && f.MovieId == movieId);
                    if (movieVote != null)
                    {
                        var movie = movieVote.Movie;

                        Db.MovieVotes.Remove(movieVote);
                        Db.SaveChanges();

                        UpdateMovieVotes(movie);
                    }
                }
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Updates the aggregated values of a movie, regarding likes and hates
        /// </summary>
        /// <param name="movie">The movie to update</param>
        private void UpdateMovieVotes(Movie movie)
        {
            movie.TotalLikes = Db.MovieVotes.Count(c => c.MovieId == movie.Id && c.IsPositive);
            movie.TotalHates = Db.MovieVotes.Count(c => c.MovieId == movie.Id && !c.IsPositive);
            Db.SaveChanges();
        }
    }
}
