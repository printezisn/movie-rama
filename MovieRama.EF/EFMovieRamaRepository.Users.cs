﻿using MovieRama.Core.Helpers;
using MovieRama.Core.Models;
using MovieRama.Resources.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.EF
{
    public partial class EFMovieRamaRepository
    {
        private static object _registrationLock = new object();
        private const string PASSWORD_SALT = "{CE016D05-B2AF-4428-88C3-1968075842ED}";

        /// <summary>
        /// Creates a new user (registration)
        /// </summary>
        /// <param name="model">The model of the new user (registration model)</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> CreateUser(RegistrationModel model)
        {
            List<string> errors = new List<string>();

            try
            {
                lock (_registrationLock)
                {
                    if (model.Password != model.ConfirmPassword)
                    {
                        errors.Add(MessagesResource.PasswordNotConfirmedErrorMessage);
                    }

                    // The username and email address must be unique
                    if (Db.Users.Any(a => a.Username == model.Username))
                    {
                        errors.Add(MessagesResource.UsernameExistsErrorMessage);
                    }
                    if (Db.Users.Any(a => a.Email == model.Email))
                    {
                        errors.Add(MessagesResource.EmailExistsErrorMessage);
                    }
                    if (errors.Any())
                    {
                        return errors;
                    }

                    // Fill the values for the new user
                    // The password must be hashed
                    User user = new User();
                    user.Username = model.Username;
                    user.Email = model.Email;
                    user.Password = EncryptionHelper.Sha256Hash(model.Password, PASSWORD_SALT);
                    user.IsActive = true;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.CreationDateTime = DateTime.UtcNow;

                    Db.Users.Add(user);
                    Db.SaveChanges();
                }
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Returns a user
        /// </summary>
        /// <param name="username">The username of the user</param>
        /// <param name="password">The password of the user</param>
        /// <returns>The model of the user or null of he was not found</returns>
        public ProfileModel GetUser(string username, string password)
        {
            // The password is hashed
            password = EncryptionHelper.Sha256Hash(password, PASSWORD_SALT);
            var user = Db.Users.FirstOrDefault(f => f.Username == username && f.Password == password && f.IsActive);
            if (user == null)
            {
                return null;
            }

            ProfileModel model = new ProfileModel();
            model.Id = user.Id;
            model.Username = username;
            model.Email = user.Email;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;

            return model;
        }

        // <summary>
        /// Returns a user
        /// </summary>
        /// <param name="username">The username of the user</param>
        /// <returns>The model of the user or null of he was not found</returns>
        public ProfileModel GetUser(string username)
        {
            var user = Db.Users.FirstOrDefault(f => f.Username == username && f.IsActive);
            if (user == null)
            {
                return null;
            }

            ProfileModel model = new ProfileModel();
            model.Id = user.Id;
            model.Username = username;
            model.Email = user.Email;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;

            return model;
        }

        /// <summary>
        /// Updates the profile of a user
        /// </summary>
        /// <param name="model">The model of the user's profile</param>
        /// <param name="username">The username of the user</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> UpdateUser(ProfileModel model, string username)
        {
            List<string> errors = new List<string>();

            try
            {
                var user = Db.Users.FirstOrDefault(f => f.Username == username && f.IsActive);
                if (user == null)
                {
                    errors.Add(MessagesResource.UserNotFoundErrorMessage);
                    return errors;
                }

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;

                Db.SaveChanges();
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }

        /// <summary>
        /// Changes the password of a user
        /// </summary>
        /// <param name="model">The model for the change password process</param>
        /// <param name="username">The username of the user</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        public IEnumerable<string> ChangePassword(ChangePasswordModel model, string username)
        {
            List<string> errors = new List<string>();

            try
            {
                var user = Db.Users.FirstOrDefault(f => f.Username == username && f.IsActive);
                if (user == null)
                {
                    errors.Add(MessagesResource.UserNotFoundErrorMessage);
                    return errors;
                }
                if (user.Password != EncryptionHelper.Sha256Hash(model.OldPassword, PASSWORD_SALT))
                {
                    errors.Add(MessagesResource.OldPasswordInvalidErrorMessage);
                }
                if (model.NewPassword != model.ConfirmNewPassword)
                {
                    errors.Add(MessagesResource.NewPasswordNotConfirmedErrorMessage);
                }
                if (errors.Any())
                {
                    return errors;
                }

                // The password must be hashed
                user.Password = EncryptionHelper.Sha256Hash(model.NewPassword, PASSWORD_SALT);

                Db.SaveChanges();
            }
            catch
            {
                errors.Add(MessagesResource.GeneralErrorMessage);
            }

            return errors;
        }
    }
}
