﻿using MovieRama.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieRama.Core.Models;
using System.Data.Entity;
using MovieRama.Resources.Messages;

namespace MovieRama.EF
{
    /// <summary>
    /// This class implements the application's service interface and uses Entity Framework for connecting to the database
    /// </summary>
    public partial class EFMovieRamaRepository : IMovieRamaRepository
    {
        private MovieRamaEntities _db;
        /// <summary>
        /// The interface to the database (Entity Framework entities)
        /// </summary>
        protected MovieRamaEntities Db
        {
            get
            {
                if (_db == null)
                {
                    _db = new MovieRamaEntities();
                }

                return _db;
            }
        }

        /// <summary>
        /// Disposes the connection to the database
        /// </summary>
        public void Dispose()
        {
            if (_db != null)
            {
                _db.Dispose();
            }
        }

        /// <summary>
        /// Returns the default language of the application
        /// </summary>
        /// <returns>The default language of the application</returns>
        private Language GetDefaultLanguageDbObject()
        {
            var language = Db.Languages.FirstOrDefault(f => f.IsDefault && f.IsEnabled);
            if (language == null)
            {
                language = Db.Languages.FirstOrDefault(f => f.IsEnabled);
            }

            return language;
        }

        /// <summary>
        /// Returns the default language of the application
        /// </summary>
        /// <returns>The model of the default language</returns>
        public LanguageModel GetDefaultLanguage()
        {
            var language = GetDefaultLanguageDbObject();

            LanguageModel model = new LanguageModel();
            model.Id = language.Id;
            model.Name = language.Name;
            model.Culture = language.Culture;
            model.IsDefault = language.IsDefault;

            return model;
        }

        /// <summary>
        /// Returns the available languages of the application
        /// </summary>
        /// <returns>A list of models for the available languages</returns>
        public IEnumerable<LanguageModel> GetLanguages()
        {
            var languages = Db.Languages.Where(w => w.IsEnabled).ToList();
            List<LanguageModel> resultList = new List<LanguageModel>();

            foreach (var language in languages)
            {
                LanguageModel model = new LanguageModel();
                model.Id = language.Id;
                model.Name = language.Name;
                model.Culture = language.Culture;
                model.IsDefault = language.IsDefault;

                resultList.Add(model);
            }

            return resultList;
        }
    }
}
