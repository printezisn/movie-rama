﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Helpers
{
    /// <summary>
    /// This class contains common regular expressions
    /// </summary>
    public static class RegexLibrary
    {
        /// <summary>
        /// Regular expression for validating usernames
        /// </summary>
        public const string USERNAME_REGEX = @"^[\w_\.]+$";

        /// <summary>
        /// Regular expression for validating alphanumeric texts
        /// </summary>
        public const string ALPHA_NUMERIC_REGEX = @"^[\w ]+$";

        /// <summary>
        /// Regular expression for validating email addresses
        /// </summary>
        public const string EMAIL_REGEX = @"^[A-Za-z0-9\._%\+-]+@[A-Za-z0-9\.-]+\.[A-Za-z]{2,4}$";
    }
}
