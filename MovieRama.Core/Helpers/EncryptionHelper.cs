﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Helpers
{
    /// <summary>
    /// This class contains helper methods for encryption
    /// </summary>
    public static class EncryptionHelper
    {
        /// <summary>
        /// Returns the encrypted value of a text using the SHA256 encryption method
        /// </summary>
        /// <param name="text">The text to encrypt</param>
        /// <param name="salt">The salt value to use for the enryption</param>
        /// <returns>The encrypted text</returns>
        public static string Sha256Hash(string text, string salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] textBytes = Encoding.UTF8.GetBytes(text);
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            byte[] textWithSaltBytes = new byte[textBytes.Length + saltBytes.Length];

            for (int i = 0; i < textBytes.Length; i++)
            {
                textWithSaltBytes[i] = textBytes[i];
            }
            for (int i = 0; i < saltBytes.Length; i++)
            {
                textWithSaltBytes[textBytes.Length + i] = saltBytes[i];
            }

            byte[] resultBytes = algorithm.ComputeHash(textWithSaltBytes);
            StringBuilder result = new StringBuilder();

            foreach (byte resultByte in resultBytes)
            {
                result.Append(resultByte.ToString("x2"));
            }

            return result.ToString();
        }
    }
}
