﻿using MovieRama.Core.Models.Base;
using MovieRama.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for collections of movies that use paging
    /// </summary>
    public class MoviesListModel : PagedList<MovieModel>
    {
        /// <summary>
        /// The parameters used for searching and sorting movies
        /// </summary>
        public MoviesSearchModel SearchModel { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public MoviesListModel()
        {
            this.SearchModel = new MoviesSearchModel();
        }
    }
}
