﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models.Enums
{
    /// <summary>
    /// This enumeration contains the available sorting directions
    /// </summary>
    public enum SortDirection
    {
        Asc,
        Desc
    }
}
