﻿using MovieRama.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class contains parameters used for searching and sorting movies
    /// </summary>
    public class MoviesSearchModel
    {
        /// <summary>
        /// The requested page
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// The maximum number of elements that each page contains
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// The id of the user, in case of searching for the movies of a user, 
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// The sorting type used for the movies
        /// </summary>
        public MovieSortType SortBy { get; set; }

        /// <summary>
        /// The sorting direction used for the movies
        /// </summary>
        public SortDirection SortDirection { get; set; }

        /// <summary>
        /// Sanitizes the values of the parameters in order to ensure that they are valid
        /// </summary>
        public void SanitizeProperties()
        {
            if (!Enum.IsDefined(typeof(SortDirection), this.SortDirection))
            {
                this.SortDirection = SortDirection.Desc;
            }
            if (!Enum.IsDefined(typeof(MovieSortType), this.SortBy))
            {
                this.SortBy = MovieSortType.Date;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MoviesSearchModel()
        {
            this.Page = 1;
            this.SortBy = MovieSortType.Date;
            this.SortDirection = SortDirection.Desc;
        }
    }
}
