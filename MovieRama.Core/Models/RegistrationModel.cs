﻿using MovieRama.Core.Attributes;
using MovieRama.Core.Helpers;
using MovieRama.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for registering a new user
    /// </summary>
    public class RegistrationModel
    {
        /// <summary>
        /// The username of the new user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "RegistrationModel_Username_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Username_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Username_MaxLength")]
        [RegularExpression(RegexLibrary.USERNAME_REGEX, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Username_Regex")]
        public string Username { get; set; }

        /// <summary>
        /// The password of the new user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "RegistrationModel_Password_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Password_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Password_MaxLength")]
        public string Password { get; set; }

        /// <summary>
        /// The confirmation for the new user's password
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "RegistrationModel_ConfirmPassword_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_ConfirmPassword_Required")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// The email address of the new user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "RegistrationModel_Email_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Email_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Email_MaxLength")]
        [RegularExpression(RegexLibrary.EMAIL_REGEX, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_Email_Regex")]
        public string Email { get; set; }

        /// <summary>
        /// The first name of the new user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "RegistrationModel_FirstName_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_FirstName_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_FirstName_MaxLength")]
        //[RegularExpression(RegexLibrary.ALPHA_NUMERIC_REGEX, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_FirstName_Regex")]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the new user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "RegistrationModel_LastName_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_LastName_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_LastName_MaxLength")]
        //[RegularExpression(RegexLibrary.ALPHA_NUMERIC_REGEX, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "RegistrationModel_LastName_Regex")]
        public string LastName { get; set; }
    }
}
