﻿using MovieRama.Core.Attributes;
using MovieRama.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for changing the password of the current user
    /// </summary>
    public class ChangePasswordModel
    {
        /// <summary>
        /// The old password of the current user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ChangePasswordModel_OldPassword_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ChangePasswordModel_OldPassword_Required")]
        public string OldPassword { get; set; }

        /// <summary>
        /// The new password of the current user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ChangePasswordModel_NewPassword_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ChangePasswordModel_NewPassword_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ChangePasswordModel_NewPassword_MaxLength")]
        public string NewPassword { get; set; }

        /// <summary>
        /// The confirmation for the new password of the current user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ChangePasswordModel_ConfirmNewPassword_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ChangePasswordModel_ConfirmNewPassword_Required")]
        public string ConfirmNewPassword { get; set; }
    }
}
