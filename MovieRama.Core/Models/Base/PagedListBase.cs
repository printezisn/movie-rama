﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models.Base
{
    /// <summary>
    /// This class is used for keeping collections that use paging
    /// </summary>
    public class PagedListBase
    {
        /// <summary>
        /// The current page
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// The maximum number of elements that each page contains
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// The number of available pages
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Indicates if there is a page before the current page
        /// </summary>
        public bool HasPreviousPage
        {
            get
            {
                return this.Page > 1;
            }
        }

        /// <summary>
        /// Indicates if there is a page after the current page
        /// </summary>
        public bool HasNextPage
        {
            get
            {
                return this.Page < this.TotalPages;
            }
        }

        /// <summary>
        /// Indicates if the current page is the first page
        /// </summary>
        public bool IsFirstPage
        {
            get
            {
                return this.Page == 1;
            }
        }

        /// <summary>
        /// Indicates if the current page is the last page
        /// </summary>
        public bool IsLastPage
        {
            get
            {
                return this.Page == this.TotalPages;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PagedListBase()
        {
            this.Page = 1;
            this.PageSize = 10;
        }
    }
}
