﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models.Base
{
    /// <summary>
    /// This class is used for keeping collections that use paging
    /// </summary>
    /// <typeparam name="T">The type of values</typeparam>
    public class PagedList<T> : PagedListBase
    {
        /// <summary>
        /// The list of values
        /// </summary>
        public IList<T> Collection { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public PagedList()
        {
            this.Collection = new List<T>();
        }
    }
}
