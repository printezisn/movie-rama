﻿using MovieRama.Core.Attributes;
using MovieRama.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for logging in a user
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// The username of the user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "LoginModel_Username_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "LoginModel_Username_Required")]
        public string Username { get; set; }

        /// <summary>
        /// The password of the user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "LoginModel_Password_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "LoginModel_Password_Required")]
        public string Password { get; set; }

        /// <summary>
        /// Indicates if the application should remember the credentials of the user after session timeout
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "LoginModel_RememberMe_Display")]
        public bool RememberMe { get; set; }
    }
}
