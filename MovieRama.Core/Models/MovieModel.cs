﻿using MovieRama.Core.Attributes;
using MovieRama.Core.Helpers;
using MovieRama.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for the movies of the application
    /// </summary>
    public class MovieModel
    {
        /// <summary>
        /// The id of the movie
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The title of the movie
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "MovieModel_Title_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "MovieModel_Title_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "MovieModel_Title_MaxLength")]
        public string Title { get; set; }

        /// <summary>
        /// The description of the movie
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "MovieModel_Description_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "MovieModel_Description_Required")]
        public string Description { get; set; }

        /// <summary>
        /// The full name of the movie's creator
        /// </summary>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// The id of the movie's creator
        /// </summary>
        public int CreatorId { get; set; }

        /// <summary>
        /// The number of likes that the movie has received
        /// </summary>
        public int TotalLikes { get; set; }

        /// <summary>
        /// The number of hates that the movie has received
        /// </summary>
        public int TotalHates { get; set; }

        /// <summary>
        /// The date that the movie was created
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Indicates if the current user has liked the movie
        /// </summary>
        public bool CurrentUserHasLiked { get; set; }

        /// <summary>
        /// Indicates if the current user has hated the movie
        /// </summary>
        public bool CurrentUserHasHated { get; set; }
    }
}
