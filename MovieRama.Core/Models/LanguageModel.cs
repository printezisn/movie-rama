﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for the available languages of the application
    /// </summary>
    public class LanguageModel
    {
        /// <summary>
        /// The id of the language
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the language
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The culture of the language (en, el, de, etc.)
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// Indicates if the language is the default language of the application
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
