﻿using MovieRama.Core.Attributes;
using MovieRama.Core.Helpers;
using MovieRama.Resources.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Models
{
    /// <summary>
    /// This model class is used for editing the profile of the current user
    /// </summary>
    public class ProfileModel
    {
        /// <summary>
        /// The id of the user
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The username of the user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ProfileModel_Username_Display")]
        public string Username { get; set; }

        /// <summary>
        /// The email address of the user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ProfileModel_Email_Display")]
        public string Email { get; set; }

        /// <summary>
        /// The first name of the user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ProfileModel_FirstName_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ProfileModel_FirstName_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ProfileModel_FirstName_MaxLength")]
        //[RegularExpression(RegexLibrary.ALPHA_NUMERIC_REGEX, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ProfileModel_FirstName_Regex")]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the user
        /// </summary>
        [LocalizedDisplayName(typeof(ModelsResource), "ProfileModel_LastName_Display")]
        [Required(ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ProfileModel_LastName_Required")]
        [MaxLength(250, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ProfileModel_LastName_MaxLength")]
        //[RegularExpression(RegexLibrary.ALPHA_NUMERIC_REGEX, ErrorMessageResourceType = typeof(ModelsResource), ErrorMessageResourceName = "ProfileModel_LastName_Regex")]
        public string LastName { get; set; }
    }
}
