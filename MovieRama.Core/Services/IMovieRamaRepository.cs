﻿using MovieRama.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Services
{
    /// <summary>
    /// The interface for the application's service
    /// </summary>
    public interface IMovieRamaRepository : IDisposable
    {
        /// <summary>
        /// Returns the default language of the application
        /// </summary>
        /// <returns>The model of the default language</returns>
        LanguageModel GetDefaultLanguage();

        /// <summary>
        /// Returns the available languages of the application
        /// </summary>
        /// <returns>A list of models for the available languages</returns>
        IEnumerable<LanguageModel> GetLanguages();

        /// <summary>
        /// Returns the available movies of the application
        /// </summary>
        /// <param name="culture">The culture used for getting localized values of movies</param>
        /// <param name="searchModel">The parameters used for searching and sorting movies</param>
        /// <param name="currentUserId">The id of the current user</param>
        /// <returns>A list of models for the movies found, using paging</returns>
        MoviesListModel GetMovies(string culture, MoviesSearchModel searchModel, int? currentUserId);

        /// <summary>
        /// Returns a movie
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="languageId">The id of the language used for getting localized values of the movie</param>
        /// <returns>A model of the movie found, or null of it was not found</returns>
        MovieModel GetMovie(int movieId, int? languageId);

        /// <summary>
        /// Indicates if a user has created a movie
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="username">The username of the user</param>
        /// <returns>Returns true if the user has created the movie, otherwise false</returns>
        bool UserHasCreatedMovie(int movieId, string username);

        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="model">The model of the new movie</param>
        /// <param name="creatorUsername">The username of the new movie's creator</param>
        /// <returns>A list of errors. If no errors occured, this list is empty. If it is successful the id in the model takes the id of the new movie</returns>
        IEnumerable<string> CreateMovie(MovieModel model, string creatorUsername);

        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="model">The model of the movie</param>
        /// <param name="languageId">The id of the language. It is used to update localized values of the movie</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> UpdateMovie(MovieModel model, int? languageId);

        /// <summary>
        /// Deletes a movie
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> DeleteMovie(int movieId);

        /// <summary>
        /// Creates a new user (registration)
        /// </summary>
        /// <param name="model">The model of the new user (registration model)</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> CreateUser(RegistrationModel model);

        /// <summary>
        /// Returns a user
        /// </summary>
        /// <param name="username">The username of the user</param>
        /// <param name="password">The password of the user</param>
        /// <returns>The model of the user or null of he was not found</returns>
        ProfileModel GetUser(string username, string password);

        // <summary>
        /// Returns a user
        /// </summary>
        /// <param name="username">The username of the user</param>
        /// <returns>The model of the user or null of he was not found</returns>
        ProfileModel GetUser(string username);

        /// <summary>
        /// Updates the profile of a user
        /// </summary>
        /// <param name="model">The model of the user's profile</param>
        /// <param name="username">The username of the user</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> UpdateUser(ProfileModel model, string username);

        /// <summary>
        /// Changes the password of a user
        /// </summary>
        /// <param name="model">The model for the change password process</param>
        /// <param name="username">The username of the user</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> ChangePassword(ChangePasswordModel model, string username);

        /// <summary>
        /// Adds a vote for a movie
        /// </summary>
        /// <param name="userId">The id of the user that votes</param>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="isPositive">This parameter is true for liking or false for hating</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> VoteForMovie(int userId, int movieId, bool isPositive);

        /// <summary>
        /// Removes a vote from a movie
        /// </summary>
        /// <param name="userId">The id of the user to remove the vote for</param>
        /// <param name="movieId">The id of the movie</param>
        /// <returns>A list of errors. If no errors occured, this list is empty</returns>
        IEnumerable<string> UnVoteForMovie(int userId, int movieId);
    }
}
