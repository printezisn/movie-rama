﻿using MovieRama.Resources.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core.Extensions
{
    /// <summary>
    /// This class contains extensions for datetime objects
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Returns a relative value for the datetime object (1 hour ago, 2 days ago, etc.)
        /// </summary>
        /// <param name="dateTime">The datetime object</param>
        /// <returns>The relative value</returns>
        public static string ToRelativeString(this DateTime dateTime)
        {
            int second = 1;
            int minute = 60 * second;
            int hour = 60 * minute;
            int day = 24 * hour;
            int month = 30 * day;

            var ts = new TimeSpan(DateTime.UtcNow.Ticks - dateTime.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * minute)
            {
                return (ts.Seconds == 1) ? MessagesResource.ASecondAgo : string.Format(MessagesResource.XSecondsAgo, ts.Seconds);
            }
            if (delta < 2 * minute)
            {
                return MessagesResource.AMinuteAgo;
            }
            if (delta < 45 * minute)
            {
                return string.Format(MessagesResource.XMinutesAgo, ts.Minutes);
            }
            if (delta < 90 * minute)
            {
                return MessagesResource.AnHourAgo;
            }
            if (delta < 24 * hour)
            {
                return string.Format(MessagesResource.XHoursAgo, ts.Hours);
            }
            if (delta < 48 * hour)
            {
                return MessagesResource.Yesterday;
            }
            if (delta < 30 * day)
            {
                return string.Format(MessagesResource.XDaysAgo, ts.Days);
            }
            if (delta < 12 * month)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return (months <= 1) ? MessagesResource.AMonthAgo : string.Format(MessagesResource.XMonthsAgo, months);
            }

            int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
            return (years <= 1) ? MessagesResource.AYearAgo : string.Format(MessagesResource.XYearsAgo, years);
        }
    }
}
