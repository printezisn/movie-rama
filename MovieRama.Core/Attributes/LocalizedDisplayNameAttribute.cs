﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieRama.Core.Attributes
{
    /// <summary>
    /// This attribute is used to translate the display name of model properties
    /// </summary>
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        /// <summary>
        /// The type of the resource
        /// </summary>
        public Type ErrorMessageResourceType { get; set; }

        /// <summary>
        /// The name of the resource's term
        /// </summary>
        public string ErrorMessageResourceName { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="errorMessageResourceType">The type of the resource</param>
        /// <param name="errorMessageResourceName">The name of the resource's term</param>
        public LocalizedDisplayNameAttribute(Type errorMessageResourceType, string errorMessageResourceName)
            : base(GetMessageFromResource(errorMessageResourceType, errorMessageResourceName))
        {
            this.ErrorMessageResourceType = errorMessageResourceType;
            this.ErrorMessageResourceName = errorMessageResourceName;
        }

        /// <summary>
        /// The translated display name of the model property
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return GetMessageFromResource(this.ErrorMessageResourceType, this.ErrorMessageResourceName);
            }
        }

        /// <summary>
        /// Returns the value of the resource's term
        /// </summary>
        /// <param name="errorMessageResourceType">The type of the resource</param>
        /// <param name="errorMessageResourceName">The name of the resource's term</param>
        /// <returns>The value of the resource's term</returns>
        private static string GetMessageFromResource(Type errorMessageResourceType, string errorMessageResourceName)
        {
            ResourceManager resourceManager = new ResourceManager(errorMessageResourceType);
            return resourceManager.GetString(errorMessageResourceName, Thread.CurrentThread.CurrentUICulture);
        }
    }
}
