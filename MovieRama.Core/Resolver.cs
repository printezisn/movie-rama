﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieRama.Core
{
    /// <summary>
    /// This class is used for implementing dependency injection
    /// </summary>
    public class Resolver
    {
        private static Resolver _instance;
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static Resolver Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Resolver();
                }

                return _instance;
            }
        }

        private static Dictionary<Type, Type> _mappings;

        /// <summary>
        /// Constructor
        /// </summary>
        private Resolver()
        {
            _mappings = new Dictionary<Type, Type>();
        }

        /// <summary>
        /// Maps a base type to an inherited type
        /// </summary>
        /// <typeparam name="TFrom">The base type</typeparam>
        /// <typeparam name="TTo">The inherited type</typeparam>
        public void Map<TFrom, TTo>()
        {
            _mappings[typeof(TFrom)] = typeof(TTo);
        }

        /// <summary>
        /// Resolves a base type to an inherited type
        /// </summary>
        /// <typeparam name="T">The base type</typeparam>
        /// <returns>An instance of the inherited type</returns>
        public T Resolve<T>()
        {
            if (!_mappings.ContainsKey(typeof(T)))
            {
                throw new InvalidOperationException("The type cannot be resolved.");
            }

            return (T)Activator.CreateInstance(_mappings[typeof(T)]);
        }
    }
}
